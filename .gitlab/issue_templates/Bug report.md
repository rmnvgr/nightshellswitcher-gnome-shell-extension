# My system

- Distribution (name and version):
- GNOME Shell version:
- I installed the extension:
	- [ ] Manually from the repository
	- [x] From extensions.gnome.org
	- [ ] From my distribution package manager (or it was preinstalled)

# My problem

## Description of the problem


## Steps to reproduce

1.


/label ~"bug::not confirmed"
