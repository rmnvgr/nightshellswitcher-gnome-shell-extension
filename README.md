# DISCONTINUED

The features of this extension have been included in [Night Theme Switcher](https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/).

# Night Shell Switcher

![](./screenshot.png)

Automatically toggle between your light and dark GNOME Shell theme variants.

Supports Night Light, Location Services and manual schedule.

**Important**: You must have enabled the [User Themes extension](https://extensions.gnome.org/extension/19/user-themes/).

_Do you need to change your GTK theme as well? Try [Night Theme Switcher](https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/)!_

## Theme compatibility

These shell themes have been tested and work:

- Adapta
- Arc
- Canta
- ChromeOS
- Flat-Remix
- Kimi
- Layan
- Matcha (Manjaro default)
- Materia
- Mojave
- Orchis
- Plata (Solus default)
- Pop (Pop!_OS default)
- Qogir
- Simply Circles
- Teja
- Vimix
- Yaru (Ubuntu default)

Other themes with a dark variant might work as well, let me know if there is a specific theme you'd like supported! And you can also manually set your day and night theme.

## Graphical installation

Visit [the extension page on extensions.gnome.org](https://extensions.gnome.org/extension/2356/night-shell-switcher/) and enable the extension.

## Command line installation

You will need these tools:

- `make`
- `gettext`
- `gnome-extensions` (comes with GNOME Shell >= 3.34)

Clone the repository and enter the directory:

```bash
git clone https://gitlab.com/rmnvgr/nightshellswitcher-gnome-shell-extension.git && cd nightshellswitcher-gnome-shell-extension
```

Build and install using `make`:

```bash
make build && make install
```

Restart your GNOME session and enable the extension:

```bash
gnome-extensions enable nightshellswitcher@romainvigier.fr
```

## Contributing

You're welcome to contribute to the code or the translations! See [CONTRIBUTING.md](./CONTRIBUTING.md).
